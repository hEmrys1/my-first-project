package ru.tinkoff.fintech;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static List<Student> studentList = new ArrayList<>();

    public static void main( String[] args ) {
        addSomeStudents();

        int sumOfAgeKirill = calcSumOfAgeByName("Kirill");

        Set<String> nameSet = listToSetNames();

        boolean result = hasStudentWithAgeMoreThan(25);

        Map<Integer, String> studentMapIdName = listToMapIdName();

        Map<Integer, List<Student>> studentMapAgeStudentList = listToMapAgeStudentList();

        System.out.println(sumOfAgeKirill);
        System.out.println("Set of names:");
        for(String name : nameSet) {
            System.out.println(name);
        }
        System.out.println(result);
        System.out.println("Map Id - Name:");
        for (Map.Entry<Integer, String> entry : studentMapIdName.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        System.out.println("Map Age - StudentList:");
        for (Map.Entry<Integer, List<Student>> entry : studentMapAgeStudentList.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }

    public static void addSomeStudents() {
        studentList.add(new Student("Kirill", 20));
        studentList.add(new Student("Alexander", 25));
        studentList.add(new Student("Vladimir", 24));
        studentList.add(new Student("Egor", 22));
        studentList.add(new Student("Irina", 24));
        studentList.add(new Student("Vladimir", 20));
        studentList.add(new Student("Kirill", 21));
        studentList.add(new Student("Vladislav", 28));
        studentList.add(new Student("Vladislava", 23));
        studentList.add(new Student("Daniil", 40));
    }

    public static int calcSumOfAgeByName(String name) {
        return studentList.stream()
                .filter(s -> Objects.equals(s.getName(), name))
                .map(Student::getAge)
                .reduce(Integer::sum)
                .orElse(0);
    }

    public static Set<String> listToSetNames() {
        return studentList.stream()
                .map(Student::getName)
                .collect(Collectors.toSet());
    }

    public static boolean hasStudentWithAgeMoreThan(int target) {
        return studentList.stream()
                .map(Student::getAge)
                .anyMatch(age -> age > target);
    }

    public static Map<Integer, String> listToMapIdName() {
        return studentList.stream()
                .collect(Collectors.toMap(Student::getId, Student::getName));
    }

    public static Map<Integer, List<Student>> listToMapAgeStudentList() {
        return studentList.stream()
                .collect(Collectors.groupingBy(Student::getAge));
    }
}
