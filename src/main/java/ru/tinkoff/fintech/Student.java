package ru.tinkoff.fintech;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Student {
    private int id;
    @Setter
    private String name;
    @Setter
    private int age;
    private static int counter;

    Student(String name, int age) {
        this.id = counter++;
        this.name = name;
        this.age = age;
    }
}
